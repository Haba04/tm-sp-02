<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12.02.2020
  Time: 11:20
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="navbar.jsp" %>
    <title>Project Edit</title>
</head>
<body>

<spring:form method="post" modelAttribute="project" action="projectCreate">
    <spring:hidden path="id"/>
    Name: <spring:input path="name"/> <br/>
    Description: <spring:input path="description"/>   <br/>
    <spring:hidden path="status"/>
    DateBegin: <spring:input type="text" class= "date" path="dateBegin"/>   <br/>
    DateEnd: <spring:input type="text" class= "date"  path="dateEnd"/>   <br/>
    <spring:button>Confirm</spring:button>
</spring:form>

</body>
</html>
